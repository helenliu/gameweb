$(window).ready(function () {
    var mySwiper = new Swiper('.hotgame-inner', {
        speed: 300,
        loop: true,
        autoplay: true,
        effect: 'slide',
    });

});

var thisBanner = (function () {
    var i = 0;
    var speed = 300;
    var time = 1000;
    var items = $(".banner > img");
    var px = ($(window).width() / 750) * 100;
    if ($(window).width() > 750) px = 100;

    var thisMain = function () {
        var i_02 = i + 1;
        var i_03 = i + 2;
        var i_04 = i + 3;
        var i_05 = i + 4;
        if (i_02 > 4) i_02 -= 5;
        if (i_03 > 4) i_03 -= 5;
        if (i_04 > 4) i_04 -= 5;
        if (i_05 > 4) i_05 -= 5;
        items.eq(i).animate(
            {
                width: 1.3 * px,
                left: 5.08 * px,
                zIndex: 1
            },
            speed
        );
        items.eq(i_02).animate(
            {
                width: 1.3 * px,
                left: 0,
                zIndex: 2
            },
            speed
        );
        items.eq(i_03).animate(
            {
                width: 1.6 * px,
                left: 0.89 * px,
                zIndex: 3
            },
            speed
        );
        items.eq(i_04).animate(
            {
                width: 1.8 * px,
                left: 2.26 * px,
                zIndex: 4
            },
            speed
        );
        items.eq(i_05).animate(
            {
                width: 1.6 * px,
                left: 3.98 * px,
                zIndex: 3
            },
            speed
        );
        i++;

        if (i > 4) i = 0;
    };

    setInterval(thisMain, time);
})();